#!/bin/bash

source 00config.sh

REFERENCE_ID="piControl"
YEAR='2020'
WORKDIR="${DATADIR}/${REFERENCE_ID}"

TEMP_INC=1  # Temperature increase in K

# Copy atmospheric inital conditions
mkdir -p "${DATADIR}/${REFERENCE_ID}-p${TEMP_INC}K/initial_condition"

ATMO_IN="${DATADIR}/${REFERENCE_ID}/initial_condition/echam2icon_${YEAR}${MONTH}${DAY}00.nc"
ATMO_OUT="${DATADIR}/${REFERENCE_ID}-p${TEMP_INC}K/initial_condition/echam2icon_${YEAR}${MONTH}${DAY}00.nc"

cdo \
  "${CDO_OPTS[@]}" \
  -aexpr,"T=T+${TEMP_INC}" \
  $ATMO_IN \
  $ATMO_OUT

# Increase land initial surface temperature
LAND_IN="${DATADIR}/${REFERENCE_ID}/initial_condition/echam2icon_ic_land_soil_${YEAR}${MONTH}.nc"
LAND_OUT="${DATADIR}/${REFERENCE_ID}-p${TEMP_INC}K/initial_condition/echam2icon_ic_land_soil_${YEAR}${MONTH}.nc"

cdo \
  "${CDO_OPTS[@]}" \
  -aexpr,"surf_temp=surf_temp+${TEMP_INC}" \
  $LAND_IN \
  $LAND_OUT

# Increase SST boundary conditions
SST_IN="${DATADIR}/${REFERENCE_ID}/sst_and_seaice/"

SST_OUT="${DATADIR}/${REFERENCE_ID}-p${TEMP_INC}K/sst_and_seaice/"
mkdir -p "${SST_OUT}"

for ncfile in ${SST_IN}/mpiesm1_bc_sst-sic*; do
  ncbase=$(basename $ncfile)
  cdo \
    "${CDO_OPTS[@]}" \
    -aexpr,"SST=SST+${TEMP_INC}" \
    "${SST_IN}/${ncbase}" \
    "${SST_OUT}/${ncbase}"
done

for ncfile in ${SST_IN}/mpiesm1_bc_amip_sst*; do
  ncbase=$(basename $ncfile)
  cdo \
    "${CDO_OPTS[@]}" \
    -expr,"SST=SST+${TEMP_INC}" \
    "${SST_IN}/${ncbase}" \
    "${SST_OUT}/${ncbase}"
done
