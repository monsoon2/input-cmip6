#!/usr/bin/env python3
import sys

import netCDF4
import numpy as np


ncfile = sys.argv[1]

with netCDF4.Dataset(ncfile, "a") as root:
    root.createDimension("nhyi", size=root.dimensions["lev"].size + 1)
    root.createVariable("nhyi", float, dimensions=("nhyi",))
    root["nhyi"].axis = "Z"  # Make CDO aware that this is a vertical coordinate

    root.createVariable("hyai", float, dimensions=("nhyi",))
    root["hyai"][:] = np.hstack((root["ap_bnds"][:, 0], root["ap_bnds"][-1, 1]))

    root.createVariable("hybi", float, dimensions=("nhyi",))
    root["hybi"][:] = np.hstack((root["b_bnds"][:, 0], root["b_bnds"][-1, 1]))
