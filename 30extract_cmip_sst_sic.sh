#!/bin/bash
#
# Extract sea surface temperature SST and sea-ice concentration SIC from CMIP6.
#
# Info: The script uses the full 3-hourly resolution of the SST data set.
#   Daily SIC values are interpolated to match the time series.
#
# Usage:
#    $ extract_cmip_sst_sic.sh FIRST_YYYY LAST_YYYY

#SBATCH --job-name=extract_sst_sic
#SBATCH --partition=compute,compute2,shared,gpu
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=1280
#SBATCH --time=06:00:00
#SBATCH --mail-type=NONE
#SBATCH --account=mh0287
#SBATCH --output=logs/extract_sst.run.%j
#SBATCH --error=logs/extract_sst.run.%j
#SBATCH --parsable

source 00config.sh

firstyear=$(( $YEAR - 5 ))
lastyear=$(( $YEAR + 4 ))

# Create working directory and change into input directory.
mkdir -p "${WORKDIR}" "${SST_SIC_OUTDIR}"

# Sea surface temperature.
# The SST data is quite large so only read the current as well as the adjacent
# decases to speedup the extraction.
decade="${YEAR:2:1}"  # 1976 -> 7
adjacent_yy="$(($decade - 1))$decade$(($decade + 1))"  # 7 -> 678

CMIP_SST="3hr/tos/gn/v20190710/tos_3hr_MPI-ESM1-2-HR_${CMIP_EXP}_${CMIP_VAR}_gn_20[${adjacent_yy}]*.nc"
CMIP_SIC="SIday/siconc/gn/v20190710/siconc_SIday_MPI-ESM1-2-HR_${CMIP_EXP}_${CMIP_VAR}_gn_*"

# Merge SST-SIC file
cdo \
  "${CDO_OPTS[@]}" \
  -splityear \
  -setpartabn,cmip2ifs.nml \
  -merge \
  -addc,273.15 \
  -selyear,"${firstyear}/${lastyear}" \
  -mergetime \
  "${CMIP_DIR}/${CMIP_SST}" \
  -divc,100.0 \
  -selyear,"${firstyear}/${lastyear}" \
  -inttime,${firstyear}-01-01,12:00:00,3hour \
  -mergetime \
  "${CMIP_DIR}/${CMIP_SIC}" \
  "${SST_SIC_OUTDIR}/sst-sic_"

# Perform a 7-day (7 * 8 hour) running mean
for ncfile in "${SST_SIC_OUTDIR}/sst-sic_"*; do
  bash 70running_mean.sh $ncfile 56
done
