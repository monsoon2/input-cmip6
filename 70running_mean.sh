#!/bin/bash
#
# Perform a running mean over a fixed time at each day (e.g. 06:00 am).
#
# Usage:
#    $ extract_cmip_sst_sic.sh NCFILE

#SBATCH --job-name=daytime_aware_running_mean
#SBATCH --partition=compute,compute2,shared,gpu
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=1280
#SBATCH --time=01:00:00
#SBATCH --mail-type=NONE
#SBATCH --account=mh0287
#SBATCH --output=logs/running_mean.%j
#SBATCH --error=logs/running_mean.%j
#SBATCH --parsable

source 00config.sh

NCFILE="${1}"
NDAYS="${2:-7}"

# Perform an individual running mean per hour of day (e.g. 3am, 6am, ...)
# and merge the separate running means into a combined dataset.
cdo "${CDO_OPTS[@]}" -splithour "${NCFILE}" "${NCFILE}-h"
cdo "${CDO_OPTS[@]}" -mergetime -apply,-runmean,$NDAYS \[ "${NCFILE}-h"* \] "${NCFILE}"

rm "${NCFILE}-"*
