#!/bin/bash

source 00config.sh

mkdir -p "${INITIAL_OUTDIR}"

DATE="${YEAR}-${MONTH}-${DAY}"
OUTFILE="${INITIAL_OUTDIR}/echam2icon_${YEAR}${MONTH}${DAY}00.nc"
VARIABLES=(ua va wap ta hus clw cli ps orog)

for param in ${VARIABLES[@]}; do
  # WARNING: Data is stored in 5-year chunks!
  REGEX="/gn/v20190710/${param}_*gn_${YEAR}*.nc"
  case $param in
    cli|clw)
      CMIP_TABLE="Amon"
      SELTIME="-selmon,${MONTH} -selyear,${YEAR}"
      ;;
    orog)
      CMIP_TABLE="fx"
      SELTIME=""
      REGEX="/gn/v20190710/${param}_*.nc"
      ;;
    *)
      CMIP_TABLE="CFday"
      SELTIME="-seldate,${DATE}"
      ;;
  esac

  cdo \
    "${CDO_OPTS[@]}" \
    $([[ "${param}" == 'ps' ]] && echo '-expr,lnps=ln(ps)') \
    $([[ "${param}" != 'ps' ]] && echo '-delname,ps') \
    $([[ "${param}" == 'orog' ]] && echo "-setdate,${DATE} -expr,gp=orog*9.80665") \
    -selname,"${param}" \
    ${SELTIME} \
    "${CMIP_DIR}/${CMIP_TABLE}/${param}/"${REGEX} \
    "${INITIAL_OUTDIR}/${param}-init.nc"  # Extract variables in parallel
done

wait  # Wait for all parallel variable extractions to finish

# Merge all variable fields into a single dataset.
cdo \
  "${CDO_OPTS[@]}" \
  -setattribute,"CMIP6_DATA_PATH"=$CMIP_DIR \
  -setpartabn,cmip2ifs.nml \
  -invertlev \
  -merge \
  "${INITIAL_OUTDIR}/"*"-init.nc" \
  "${OUTFILE}"

# Move hybrid coefficients a and b in a one-dimensional array with dimension
# [plev +1] instead of a two-dimensional [plev + 1, bounds].
python 11restack_hybrid_coeffs.py "${OUTFILE}"

rm "${INITIAL_OUTDIR}/"*"-init.nc"
