#!/usr/bin/env python3
"""Apparently JSBACH4 requires a yearly climaotoloy as input.

This script duplicates the intial condition extracted from CMIP6
and adds an artifical time axis that represents days of year.

DISLAIMER:
    I have no idea if this is the actual reasoning. I just found
    that input using a single timestep results in segmentation faults.
"""
import sys
import netCDF4

infile = sys.argv[1]

time = [30.,  58.,  89., 119., 150., 180., 211., 242., 272., 303., 333., 364.]

with netCDF4.Dataset(infile, "a") as root:
    root["time"][:] = time
    root["time"].units = "days since 1-1-1"
    root["time"].calendar = "standard"

    for i in range(len(time)):
        root["surf_temp"][i, :] = root["surf_temp"][0, :]
