#!/bin/bash
#
# Remap all data fields in a INFILE to a new grid.
#
# Info:
#   Interpolation weights are stored to accelearte future interpolations.
#
# Usage:
#    $ remap.sh INFILE

#SBATCH --job-name=remap
#SBATCH --partition=compute,compute2,gpu
#SBATCH --exclusive
#SBATCH --constraint=512G
#SBATCH --mem=0
#SBATCH --time=02:00:00
#SBATCH --mail-type=FAIL
#SBATCH --account=mh0287
#SBATCH --output=logs/remap.run.%j
#SBATCH --error=logs/remap.run.%j

source 00config.sh

OUTFILE="${1%.*}_${GRID_ID}_${GRID_REFINEMENT}_${GRID_LABEL}.nc"

CDO_OPTS+=(-P 32)

# Determine first gridtype in input data (e.g. gaussian, curvilinear)
gridtype=$(cdo -griddes "${1}" | grep -m 1 gridtype | cut -b 13-)

# Calculate interpilation weights once. The weights are stored for each
# combination of interpolation order, input grid, and output grid.
mkdir -p "${WEIGHTS_OUTDIR}"
REMAP_WEIGHTS="${WEIGHTS_OUTDIR}/${REMAP_INTERP}_${gridtype}_${GRID_ID}_${GRID_REFINEMENT}_${GRID_LABEL}.nc"
if [[ ! -f "${REMAP_WEIGHTS}" ]]; then
  # If not present, create interpolation weights for future use.
  cdo \
    "${CDO_OPTS[@]}" \
    "-gen${REMAP_INTERP},${GRID_FILE}" \
    -setmisstonn \
    "${1}"\
    "${REMAP_WEIGHTS}"
fi

# Interpolate gridded CMIP6 data (~250km) to ICON grid defined in `config.sh`.
cdo \
  "${CDO_OPTS[@]}" \
  -remap,"${GRID_FILE}","${REMAP_WEIGHTS}" \
  -setmisstonn \
  "${1}"\
  "${OUTFILE}"

# ICON expects IFS input to have a `ncells` dimensions instead of `cell`.
[[ $(basename ${1}) == echam2icon* ]] && ncrename -d cell,ncells "${OUTFILE}"

echo "${OUTFILE}"
