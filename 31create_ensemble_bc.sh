#!/bin/bash
#
# Create direct boundary conditions.
#
# The SST and sea-ice data consist of 3-hourly data in a merged file.
# In contrast to the monthly AMIP data these boundary conditions are used to
# force the model directly (e.g. with a diurnal cycle).
#

source 00config.sh

CDO_OPTS+=(-P 8)

firstyear=$(( $YEAR - 4 ))
lastyear=$(( $YEAR + 3 ))
baseyear=$YEAR

start_season=03
end_season=10

cd ${SST_SIC_OUTDIR} || exit 1

exp_id=0
for year in $(seq $firstyear $lastyear); do
  # For each year, shift the time axis to 2020.
  cdo \
    "${CDO_OPTS[@]}" \
    -settunits,hours \
    -shifttime,$(( $baseyear - $year ))years, \
    -selmonth,${start_season}/${end_season} \
    "sst-sic_${year}.nc" \
    "mpiesm1_bc_sst-sic_r${exp_id}.nc"

  exp_id=$((exp_id + 1))
done
