#!/bin/bash
#
# Load site-specific settings based on hostname.

case $(hostname) in

  mistral*|mlogin*|m*)
    source 00config.mistral
    ;;

  *.juwels)
    source 00config.juwels
    ;;
esac
