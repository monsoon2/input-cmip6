#!/bin/bash

source 00config.sh

mkdir -p "${INITIAL_OUTDIR}"

OUTFILE="${INITIAL_OUTDIR}/echam2icon_ic_land_soil_${YEAR}${MONTH}.nc"
VARIABLES=(ts mrsol snw)

for param in ${VARIABLES[@]}; do
  case $param in
    "snw")
      CMIP_TABLE="LImon"
      ;;
    *)
      CMIP_TABLE="Eday"
      ;;

  esac
  REGEX="/gn/v20190710/${param}_*gn_${YEAR}*.nc"

  cdo \
    "${CDO_OPTS[@]}" \
    $([[ "${param}" == 'mrsol' ]] && echo '-expr,mrsol=mrsol/1000') \
    $([[ "${param}" == 'snw' ]] && echo '-expr,snw=snw/1000') \
    -monmean \
    -selmonth,"${MONTH}" \
    -selyear,"${YEAR}" \
    -selname,"${param}" \
    "${CMIP_DIR}/${CMIP_TABLE}/${param}/"${REGEX} \
    "${INITIAL_OUTDIR}/${param}-init.nc~"

done

# Merge all variable fields into a single dataset.
cdo \
  "${CDO_OPTS[@]}" \
  -setattribute,"CMIP6_DATA_PATH"=$CMIP_DIR \
  -setpartabn,cmip2ifs.nml \
  -merge \
  "${INITIAL_OUTDIR}/"*"-init.nc~" \
  "${OUTFILE}"

# Make input dataset JSBACH-friendly:
# 0. Convert output file into classic NetCDF-3 format due to insanity.
# http://nco.sourceforge.net/nco.html#ncrename_crd
nccopy -k nc3 ${OUTFILE} ${OUTFILE}_temp

# 1. Rename vertical soil coordinate
ncrename -d depth,soillev "${OUTFILE}_temp"
ncrename -v depth,soillev "${OUTFILE}_temp"

# 2. Remove time dimesnion from soil moisture and snow variables
ncwa -a time -v layer_moist,snow "${OUTFILE}_temp" "${INITIAL_OUTDIR}/temp1.nc"
cdo -selvar,surf_temp "${OUTFILE}_temp" "${INITIAL_OUTDIR}/temp2.nc"
cdo "${CDO_OPTS[@]}" -merge "${INITIAL_OUTDIR}/temp"?".nc" "${OUTFILE}_temp"

# 0. Convert output file back into NetCDF-4 format.
rm ${OUTFILE}
nccopy -d 1 -k nc4 ${OUTFILE}_temp ${OUTFILE}
rm ${OUTFILE}_temp

# 3. Fake a yearly climatology.
python 21expand_timedim_land.py "${OUTFILE}"

rm "${INITIAL_OUTDIR}/"*"-init.nc~" ${INITIAL_OUTDIR}/temp?.nc  # Remove intermediate files
