# Extract initial and boundary conditions for ICON from CMIP6

This repository contains CDO scripts that allow to extract data from CMIP6
and use them as initial and boundary conditions for ICON simulations.

The created data consists of initial conditions for the atmosphere and land
component as well as boundary conditions for sea-surface temperature (SST) and
sea-ice concentration (SIC).

In addition, the repository provides convenience scripts for remapping and
time-aware running means.

## Usage

In addition to the  processing scripts, there exists a `00config.sh`.`
This file is used for general configuration of the other scripts. It defines
general CDO options, which CMIP6 experiment to use and where to find it, and
which date to extract.

There are three scripts mainly responsible for extracting the required CMIP6
data and storing it into merged netCDF files:
```sh
bash 10extract_cmip_initial.sh  # Atmosphere initial conditions
bash 20extract_cmip_land.sh  # Land initial conditions
bash 30extract_cmip_sst_sic.sh  # SST and SIC boundary conditions
```
While extracting the data some moving average is applied to the land data and
the ocean data to minimize spurious signals at the model start.

In a next step, the SST and SIC data can be used to create 16 ensemble members.
The ensemble members are created by adding their annual anomalies on top of a
common reference year. This ensures the same base climate while capturing the
internal variablity of the climate system.
```sh
bash 31create_ensemble_bc.sh
```

## +1K runs
There exists a convenience script to increase the (sea and land) temperature of
an already processed CMIP simulations in order to create +1K data.
```sh
40increase_temperature.sh*
```


## Requirements
* Bash
* CDO
* NCO
* Python3 (netCDF4, numpy)
