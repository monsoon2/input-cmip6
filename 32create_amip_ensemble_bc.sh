#!/bin/bash
#
# Create AMIP-style boundary conditions.
#
# The SST and sea-ice data consist of monthly means in separate files.
#

source 00config.sh

CDO_OPTS+=(-P 8)

firstyear=$(( $YEAR - 4 ))
lastyear=$(( $YEAR + 3 ))
baseyear=$YEAR

cd ${SST_SIC_OUTDIR} || exit 1

# Create file with all monthly mean data
cdo \
  "${CDO_OPTS[@]}" \
  -mergetime \
  -apply,-monmean [ sst-sic_*.nc ] \
  sst-sic_monthly_means.nc

# Split monthly meaninto three year datasets (to include neighboring years).
# In addition, shift the time axis so that all ensemble members are centered
# around the baseyear.
exp_id=0
for year in $(seq $firstyear $lastyear); do
  cdo \
    "${CDO_OPTS[@]}" \
    -shifttime,$(( $baseyear - $year ))years, \
    -selyear,$(( $year - 1 ))/$(( $year + 1 )) \
    sst-sic_monthly_means.nc \
    "mpiesm1_bc_amip_sst-sic_r${exp_id}.nc~"

  cdo \
    "${CDO_OPTS[@]}" \
    -selvar,SST \
    "mpiesm1_bc_amip_sst-sic_r${exp_id}.nc~" \
    "mpiesm1_bc_amip_sst_r${exp_id}.nc"

  cdo \
    "${CDO_OPTS[@]}" \
    -selvar,SIC \
    "mpiesm1_bc_amip_sst-sic_r${exp_id}.nc~" \
    "mpiesm1_bc_amip_sic_r${exp_id}.nc"

  rm "mpiesm1_bc_amip_sst-sic_r${exp_id}.nc~"

  exp_id=$((exp_id + 1))
done

rm sst-sic_monthly_means.nc
